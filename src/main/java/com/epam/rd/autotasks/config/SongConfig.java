package com.epam.rd.autotasks.config;

import com.epam.rd.autotasks.Song;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;

/**
 * The SongConfig class is a configuration class that sets up a Song bean.
 * It reads song properties from an external properties file and uses them to create a Song object.
 * This class is annotated with @PropertySource to specify the location of the properties file.
 */
@PropertySource("classpath:application.properties")
public class SongConfig {
    /**
     * The title of the song. This is a unique identifier for the song.
     */
    @Value("${song.title}")
    private String title;

    /**
     * The artist of the song. This refers to the individual or group
     * who performed or created the song.
     */
    @Value("${song.artist}")
    private String artist;

    /**
     * The year the song was released. This provides historical context
     * for the song, indicating when it was made available to the public.
     */
    @Value("${song.year}")
    private String year;

    /**
     * Creates a Song bean with the title, artist, and year read from the properties file.
     * This method is annotated with @Bean, indicating that it produces a bean to be managed by the Spring container.
     *
     * @return a Song object with properties set from the properties file
     */
    @Bean
    public Song song() {
        return new Song(title, artist, year);
    }
}
