package com.epam.rd.autotasks;

/**
 * The Song class encapsulates the concept of a musical composition,
 * characterized by its title, artist, and the year it was released.
 */
public class Song {

    /**
     * The title of the song. This is a unique identifier for the song.
     */
    private String title;

    /**
     * The artist of the song. This refers to the individual or group
     * who performed or created the song.
     */
    private String artist;

    /**
     * The year the song was released. This provides historical context
     * for the song, indicating when it was made available to the public.
     */
    private String year;

    /**
     * Constructs a new Song with the specified title, artist, and year.
     * This constructor is used to create a complete representation of a song.
     *
     * @param title  the title of the song
     * @param artist the artist of the song
     * @param year   the year the song was released
     */
    public Song(String title, String artist, String year) {
        this.title = title;
        this.artist = artist;
        this.year = year;
    }

    /**
     * Returns the title of the song.
     *
     * @return the title of the song
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the title of the song.
     *
     * @param title the title of the song
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Returns the artist of the song.
     *
     * @return the artist of the song
     */
    public String getArtist() {
        return artist;
    }

    /**
     * Sets the artist of the song.
     *
     * @param artist the artist of the song
     */
    public void setArtist(String artist) {
        this.artist = artist;
    }

    /**
     * Returns the year the song was released.
     *
     * @return the year the song was released
     */
    public String getYear() {
        return year;
    }

    /**
     * Sets the year the song was released.
     *
     * @param year the year the song was released
     */
    public void setYear(String year) {
        this.year = year;
    }
}
