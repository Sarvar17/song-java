package com.epam.rd.autotasks;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.epam.rd.autotasks.config.SongConfig;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * The SongTest class contains unit tests for the Song class.
 * It uses the JUnit 5 framework for testing and the Spring framework for dependency injection.
 */
class SongTest {

    /**
     * This test checks the configuration of the Song bean.
     * It uses an AnnotationConfigApplicationContext to load the SongConfig class,
     * retrieves the Song bean from the context, and asserts that the bean is not null
     * and that its properties have been correctly set.
     */
    @Test
    void testSongConfiguration() {
        try (AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SongConfig.class)) {

            Song song = context.getBean(Song.class);
            assertNotNull(song, "Song should not be null");
            assertEquals("Only the young", song.getTitle(), "Song title should be \"Only the young\"");
            assertEquals("Taylor Swift", song.getArtist(), "Song artist should be \"Taylor Swift\"");
            assertEquals("2020", song.getYear(), "Song year should be \"2020\"");
        }
    }
}
